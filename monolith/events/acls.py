import json
import requests

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {"query": f"{city},{state}"}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    the_json = response.json()
    picture_url = the_json["photos"][0]["src"]["original"]
    picture_dict = {"picture_url": picture_url}
    return picture_dict


def get_lat_lon(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    the_json = response.json()
    lat = the_json[0]["lat"]
    lon = the_json[0]["lon"]
    return lat, lon


def get_weather_data(city, state):
    # Use the Open Weather API
    lat, lon = get_lat_lon(city, state)
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(url, params=params)
    the_json = response.json()
    temp = the_json["main"]["temp"]
    description = the_json["weather"][0]["description"]
    weather_dict = {
        "temp": temp,
        "description": description,
    }
    return weather_dict
